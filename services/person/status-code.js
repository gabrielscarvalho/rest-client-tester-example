function Endpoint(){
	
	var endpoint = require("rest-client-tester/endpoint.js")();
	var assert = require("rest-client-tester/util/assert");
	
	

	var id = 2;

	endpoint.retry.retryEach(500);
	
	endpoint
		.request
			.setUrl("http://demo3840477.mockable.io/person/search/id/")
			.setUrlSuffix(id)
			.setRequestType("GET")
			.setTimeOut(4000)
			.addHeader("Content-Type", "application/json");
			


	endpoint.response
		.checkResponse(function(request, response, promise) {
			
			assert.assertEquals(200, response.statusCode, "Status code is unespected.");
			assert.assertType(response.body, 'object' , 'Response should be an object');
			assert.assertEquals(parseInt(response.body.id), request.urlSuffix, "Should return the id that was send.");

			promise.resolve();
		}
	);

	
	return endpoint;

}

module.exports = Endpoint;