/**
	For this API, I've created only ids: 2 e 5.
	2 is valid and 5 has wrong data.

*/
function Endpoint(){
	
	var endpoint = require("rest-client-tester/endpoint.js")();
	var assert = require("rest-client-tester/util/assert");
	var logger = require("rest-client-tester/util/logger");

	endpoint.ref = "older";

	endpoint.retry.retryEach(500);
	
	//endpoint.event.stopWhen.hasSucceed = true;
	//endpoint.event.stopWhen.hasFailed = true;
	
	endpoint.response.config.checkResponseTimeOutMs = 10000;

	endpoint
		.request
			.setUrl("http://demo3840477.mockable.io/person/older")
			.setRequestType("GET")
			.setTimeOut(5000);
			//.addHeader("Authorization", "Basic c4...");

	endpoint.request
		.updateRequest(function(request, response, promise) {
            logger.debug("Updating search-older request");
			promise.resolve(request);
		}
	);

	
	var personEndPoint = require("./search-person")();

	endpoint.response
		.checkResponse(function(request, response, promise) {
			//search-older
			assert.assertEquals(200, response.statusCode, "Status code is unespected.");
			assert.assertType(response.body, 'object' , 'Response should be an object');
			assert.assertNotNull(parseInt(response.body.id), "Should return the oldest person.");
			
			personEndPoint.setId(parseInt(response.body.id))
				.doRequestAndAsserts().then(
					function(){
						//console.log('The person is valid!');
						assert.assertEquals("John P.", personEndPoint.response.body.name, "Expected that John was the older");
						promise.resolve();					
					}, 
					function(err){
						console.log('Is not ok! the id is invalid! %o', err);
						err.log();
						promise.reject(err);
					}).catch(function(err) {
						promise.reject(require("../../util/errorholder")().setError("Uncaught error while searching user", exc));
					}
			);

								

				


	
		}
	);

	endpoint.searchById = function(idPerson) {
		endpoint.request.setUrlSuffix(idPerson);
		return endpoint.doRequest();
	}
	
	
	return endpoint;

}

module.exports = Endpoint;